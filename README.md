This package replaces the recipient email address in development with Laravel(v7)

Install:
``` bash
$ composer require yorick/replace-mail-dev --dev
```
Publish configuration file:
``` bash
$ php artisan vendor:publish --provider="Yorick\ReplaceMailDev\CustomMailServiceProvider" --tag="config"
```
replaceEmail.php
``` php
    return [

        'emailTo' => 'example@mail.com' //change email

    ];
```

cc and bcc will not be sent.