<?php

namespace Yorick\ReplaceMailDev;

use Illuminate\Mail\MailManager;
use Illuminate\Mail\MailServiceProvider;

class CustomMailServiceProvider extends MailServiceProvider
{
    private const PROD = [
        'prod',
        'production'
    ];
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->publishes([__DIR__ . '/../config/replaceEmail.php' => base_path('replaceEmail.php')], 'config');
    }

    /**
     * @return void
     */
    public function register(): void
    {
        $this->registerIlluminateMailer();
        $this->registerMarkdownRenderer();
    }

    /**
     * @return void
     */
    protected function registerIlluminateMailer()
    {
        if (!$this->isProduction()) {
            $this->app->singleton('mail.manager', function ($app) {
                return new CustomMailManager($app);
            });

            $this->app->bind('mailer', function ($app) {
                return $app->make('mail.manger')->mailer();
            });
        } else {
            parent::registerIlluminateMailer();
        }

    }

    /**
     * @return bool
     */
    protected function isProduction(): bool
    {
        return in_array(env('APP_ENV'), self::PROD);
    }
}
