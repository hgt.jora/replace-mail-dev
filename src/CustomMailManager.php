<?php

namespace Yorick\ReplaceMailDev;

use Illuminate\Mail\MailManager;

/**
 * Class CustomMailManager
 * @package Yorick\ReplaceMailToDev
 */
class CustomMailManager extends MailManager
{
    /**
     * @param string $name
     * @return \Illuminate\Mail\Mailer
     *
     * @throws \InvalidArgumentException
     */
    protected function resolve($name)
    {
        $config = $this->getConfig($name);

        if (is_null($config)) {
            throw new InvalidArgumentException("Mailer [{$name}] is not defined.");
        }

        $mailer = new CustomMailer(
            $name,
            $this->app['view'],
            $this->createSwiftMailer($config),
            $this->app['events'],
            $this->app['config']
        );

        if ($this->app->bound('queue')) {
            $mailer->setQueue($this->app['queue']);
        }

        foreach (['from', 'reply_to', 'to', 'return_path'] as $type) {
            $this->setGlobalAddress($mailer, $config, $type);
        }

        return $mailer;
    }
}
