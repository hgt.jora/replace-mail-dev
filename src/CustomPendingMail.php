<?php

namespace Yorick\ReplaceMailDev;

use Illuminate\Mail\PendingMail;

/**
 * Class CustomPendingMail
 * @package Yorick\ReplaceMailToDev
 */
class CustomPendingMail extends PendingMail
{
    /**
     * @param  mixed  $users
     * @return $this
     */
    public function cc($users)
    {
        return $this;
    }

    /**
     * @param  mixed  $users
     * @return $this
     */
    public function bcc($users)
    {
        return $this;
    }
}
