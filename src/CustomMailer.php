<?php

namespace Yorick\ReplaceMailDev;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\PendingMail;
use InvalidArgumentException;
use Swift_Mailer;

/**
 * Class CustomMailer
 * @package Yorick\ReplaceMailToDev
 */
class CustomMailer extends Mailer
{
    protected $config;

    /**
     * CustomMailer constructor.
     * @param string $name
     * @param Factory $views
     * @param Swift_Mailer $swift
     * @param Dispatcher|null $events
     * @param $config
     */
    public function __construct(string $name, Factory $views, Swift_Mailer $swift, Dispatcher $events = null, $config)
    {
        parent::__construct($name, $views, $swift, $events);
        $this->config = $config;
    }

    /**
     * @param mixed $users
     * @return \Illuminate\Mail\PendingMail
     */
    public function to($users)
    {
        return (new CustomPendingMail($this))->to($this->getReplaceEmailTo());
    }

    /**
     * @param mixed $users
     * @return \Illuminate\Mail\PendingMail
     */
    public function cc($users)
    {
        return (new CustomPendingMail($this))->cc('');
    }

    /**
     * @param mixed $users
     * @return \Illuminate\Mail\PendingMail
     */
    public function bcc($users)
    {
        return (new CustomPendingMail($this))->bcc('');
    }

    /**
     * @return mixed
     */
    protected function getReplaceEmailTo()
    {
        $replaceEmailTo = $this->config['replaceEmail.emailTo'];
        if (is_null($replaceEmailTo)) {
            throw new InvalidArgumentException("Config 'replaceEmail' is not defined.");
        }
        return $replaceEmailTo;
    }
}
